package com.adf.Lab2ADF.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.adf.Lab2ADF.client.CurrencyClient;
import com.adf.Lab2ADF.dataclass.currency.HistoricalBaseCurrency;
import com.adf.Lab2ADF.dataclass.currency.LatestBaseCurrency;
import com.adf.Lab2ADF.util.Constant;
import com.adf.Lab2ADF.util.DateUtil;
import com.adf.Lab2ADF.util.Parser;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * CurrencyServiceImpl
 */
@Service
@Qualifier("CurrencyServiceImpl")
public class CurrencyServiceImpl implements CurrencyService {

    CurrencyClient currencyClient = new CurrencyClient();

    @Override
    public boolean isSupportedCurrencies(List<String> currencies) {
        LatestBaseCurrency euroBasedLatest = currencyClient.getLatestRate();
        for (String currency : currencies) {
            if (euroBasedLatest.getBaseRateTo(currency) == null) {
                return false;
            }
        }
        return true;
    }

    @Override
    public HistoricalBaseCurrency getComparisonHistoricAgaints(String query, String againts, Date startDate,
            Date endDate) throws IllegalArgumentException {
        List<String> processedQuery = Parser.parseStringToList(query, ",");

        List<String> allCurrenciesUsed = new ArrayList<>(processedQuery);
        allCurrenciesUsed.add(againts);

        if (isSupportedCurrencies(allCurrenciesUsed) == false || processedQuery.size() > Constant.HISTORIC_MAXIMUM_QUERY) {
            throw new IllegalArgumentException();
        }

        HistoricalBaseCurrency historicalRate = currencyClient.getHistoricalRateByBaseCurrencyAndDate(againts, 
            DateUtil.convertDateToFormat(startDate, Constant.DATE_FORMAT), 
            DateUtil.convertDateToFormat(endDate, Constant.DATE_FORMAT));
        
        filterBesidesCurrencies(processedQuery, historicalRate);
        
        return historicalRate;
    }

    /**
     * Will filter any currency inside of any of the date beside the param
     * currencies. Beware, this will modify the content of the param "data".
     * 
     * @param currencies
     * @param baseCurrency
     * @param historicalRate
     */
    private static void filterBesidesCurrencies(List<String> currencies, HistoricalBaseCurrency historicalRate) {
        Set<String> uniqueCurrency = new HashSet<>(currencies);

        Map<String, Map<String, Double>> rates = historicalRate.getRates();
        
        // Iterate through the dates
        for (String date : rates.keySet()) {
            Map<String, Double> rate = rates.get(date);

            // stream is scary because functional programming.
            // https://www.mkyong.com/java8/java-8-filter-a-map-examples/
            Map<String, Double> filteredRate = rate.entrySet().stream()
                .filter(currentRate -> uniqueCurrency.contains(currentRate.getKey()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
            
            // a side effect method called LITERALLY after a functional method. God I love Java.
            rates.put(date, filteredRate);
        }
    }
}