package com.adf.Lab2ADF.service;

import java.util.Date;
import java.util.List;

import com.adf.Lab2ADF.dataclass.currency.HistoricalBaseCurrency;

/**
 * CurrencyService
 */
public interface CurrencyService {
    boolean isSupportedCurrencies(List<String> currencies);

	/**
	 * Akan memberikan HistoricalBasedData dari tanggal startDate sampai endDate dan menyisakan currency
	 * yang ada di query.
	 * @param query
	 * @param againts
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws IllegalArgumentException when there's one or more currency that's not supported
	 */
	HistoricalBaseCurrency getComparisonHistoricAgaints(String query, String againts, Date startDate,
			Date endDate) throws IllegalArgumentException;
}