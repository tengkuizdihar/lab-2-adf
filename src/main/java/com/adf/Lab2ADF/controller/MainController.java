package com.adf.Lab2ADF.controller;

import java.text.ParseException;
import java.util.Date;

import com.adf.Lab2ADF.client.CountriesClient;
import com.adf.Lab2ADF.client.CurrencyClient;
import com.adf.Lab2ADF.client.response.GeneralResponse;
import com.adf.Lab2ADF.dataclass.currency.HistoricalBaseCurrency;
import com.adf.Lab2ADF.service.CurrencyService;
import com.adf.Lab2ADF.util.Constant;
import com.adf.Lab2ADF.util.DateUtil;
import com.adf.Lab2ADF.util.ResponseCreator;
import com.adf.Lab2ADF.util.builder.GeneralResponseFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("api")
public class MainController {



    private CurrencyClient currencyClient = new CurrencyClient();
    private CountriesClient countriesClient = new CountriesClient();

    @Autowired
    CurrencyService currencyService;

    @GetMapping("/get-region-comparison")
    public GeneralResponse getRegionalComparison(@RequestParam(name = "region")String region,
                                                        @RequestParam(name = "against")String standardCurrency,
                                                        @RequestParam(name = "start")String start,
                                                        @RequestParam(name = "end")String end) {

        return ResponseCreator.createRegionalComparisonResponse(region,standardCurrency, start, end);

    }


    // test with this url
    // http://localhost:8080/api/get-comparison?query=hai&againts=IDR&start=2018-01-01&end=2018-01-30
    @GetMapping(value = "get-comparison")
    @ResponseBody
    public GeneralResponse getComparison(@RequestParam(value = "query", required = true) String query,
            @RequestParam(value = "against", required = true) String against,
            @RequestParam(value = "start", required = true) String start,
            @RequestParam(value = "end", required = true) String end) {
        try {
            Date start_date = DateUtil.convertStringToDate(start, Constant.DATE_FORMAT);
            Date end_date = DateUtil.convertStringToDate(end, Constant.DATE_FORMAT);

            HistoricalBaseCurrency result = currencyService.getComparisonHistoricAgaints(query, against, start_date,
                    end_date);
         
            return GeneralResponseFactory.createOkResponse(result);
        } catch (ParseException e) {
            return GeneralResponseFactory.createFailedResponse(500, "Format tanggal tidak sesuai dengan " + Constant.DATE_FORMAT + ".");
        } catch (IllegalArgumentException iae) {
            return GeneralResponseFactory.createFailedResponse(404, "Maaf, mata uang tidak ditemukan.");
        }
    }
}
