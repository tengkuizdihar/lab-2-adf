package com.adf.Lab2ADF.controller;

import com.adf.Lab2ADF.client.CurrencyClient;
import com.adf.Lab2ADF.client.response.GeneralResponse;
import com.adf.Lab2ADF.service.CurrencyService;
import com.adf.Lab2ADF.util.builder.GeneralResponseFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("test")
public class TestController {

    CurrencyClient currencyClient = new CurrencyClient();

    @Autowired
    CurrencyService currencyService;

    @GetMapping("test-currency")
    @ResponseBody
    public GeneralResponse getResponse(@RequestParam(value = "currency", required = true) String currency) {
        try {
            GeneralResponse newResponse = GeneralResponseFactory
                    .createOkResponse(currencyClient.getRateAgaintsCurrency(currency));
            return newResponse;
        } catch (Exception illegalArgumenException) {
            return GeneralResponseFactory.createFailedResponse(500, "Mata uang yang anda berikan tidak kami temukan.");
        }
    }
}

