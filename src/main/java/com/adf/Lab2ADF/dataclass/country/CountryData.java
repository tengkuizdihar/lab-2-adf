package com.adf.Lab2ADF.dataclass.country;

import com.adf.Lab2ADF.dataclass.DataClass;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class CountryData implements DataClass {


    @JsonProperty("name")
    String name;

    @JsonProperty("currencies")
    List<Currency> currencies;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Currency> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(ArrayList<Currency> currencies) {
        this.currencies = currencies;
    }
}