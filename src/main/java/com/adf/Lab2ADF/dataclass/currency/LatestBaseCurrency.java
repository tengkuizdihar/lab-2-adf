package com.adf.Lab2ADF.dataclass.currency;

import java.util.Map;

import com.adf.Lab2ADF.dataclass.DataClass;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * LatestBaseCurrencyResponse
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LatestBaseCurrency implements DataClass {
    
    @JsonProperty("rates")
    Map<String, Double> rates;

    public Double getBaseRateTo(String toCurrency) {
        return rates.get(toCurrency);
    }

    public Map<String,Double> getRates() {
        return this.rates;
    }

    public void setRates(Map<String,Double> rates) {
        this.rates = rates;
    }

}