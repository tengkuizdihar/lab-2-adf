package com.adf.Lab2ADF.dataclass.currency;

import com.adf.Lab2ADF.dataclass.DataClass;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HistoricalBaseCurrency implements DataClass {
    private String base;
    private Map<String, Map<String, Double>> rates;


    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public Map<String, Map<String, Double>> getRates() {
        return rates;
    }

    public void setRates(Map<String, Map<String, Double>> rates) {
        this.rates = rates;
    }

    @JsonIgnore
    public List<String> getDatesExchangeRates() {
        return new ArrayList<>(rates.keySet());
    }
}
