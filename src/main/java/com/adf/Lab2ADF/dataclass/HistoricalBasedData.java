package com.adf.Lab2ADF.dataclass;

import java.util.Map;

/**
 * HistoricalBasedData
 */
public class HistoricalBasedData implements DataClass {

    String base;
    Map<String, Map<String, Double>> rates;

    public void addDateAndRate(String date, Map<String, Double> rate) {
        rates.put(date, rate);
    }

    public Map<String, Double> getRate(String date) {
        return rates.get(date);
    }

    public String getBase() {
        return this.base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public Map<String,Map<String,Double>> getRates() {
        return this.rates;
    }

    public void setRates(Map<String,Map<String,Double>> rates) {
        this.rates = rates;
    }
    
}