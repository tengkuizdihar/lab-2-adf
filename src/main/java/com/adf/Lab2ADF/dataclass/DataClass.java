package com.adf.Lab2ADF.dataclass;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * DataClass adalah sebuah interface yang dapat mengubah
 * class biasa yang mengimplementasinya menjadi sebuah objek
 * yang bisa dijadikan json dan mentranslasikan json secara mudah.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public interface DataClass extends Serializable {
    
}
