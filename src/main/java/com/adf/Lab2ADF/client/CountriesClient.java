package com.adf.Lab2ADF.client;

import com.adf.Lab2ADF.dataclass.country.CountryData;
import com.adf.Lab2ADF.util.Constant;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

public class CountriesClient {
    private RestTemplate restTemplate = new RestTemplate();
    private boolean validResponse = true;


    public List<CountryData> getCountryDataByRegion(String region) {
        List<CountryData> countryDataByRegion;
        countryDataByRegion = getCountryByRegionData(region);
        return countryDataByRegion;

}

    private List<CountryData> getCountryByRegionData(String region) {
        List<CountryData> countryDataByRegion;
        try {
            countryDataByRegion = Arrays.asList(getRawArrayCountryData(region));
            return countryDataByRegion;
        }catch (NullPointerException e) {
            validResponse = false;
        }
        return null;
    }
    private String getCountriesByRegionUrl(String region) {
        return Constant.COUNTRY_URI+"region/"+region;
    }

    private boolean isValidResponse() {
        return validResponse;
    }

    private CountryData[] getRawArrayCountryData(String region) {
        return restTemplate.getForObject(getCountriesByRegionUrl(region), CountryData[].class);
    }
}
