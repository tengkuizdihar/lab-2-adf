package com.adf.Lab2ADF.client.response;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * AbstractResponse
 */
public class GeneralResponse implements Serializable{

    @JsonProperty("status")
    int status;

    @JsonProperty("message")
    String message;

    @JsonProperty("result")
    Serializable result;

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Serializable getResult() {
        return this.result;
    }

    public void setResult(Serializable result) {
        this.result = result;
    }
}