package com.adf.Lab2ADF.client;

import java.util.HashMap;
import java.util.Map;

import com.adf.Lab2ADF.dataclass.currency.HistoricalBaseCurrency;
import com.adf.Lab2ADF.dataclass.currency.LatestBaseCurrency;
import com.adf.Lab2ADF.util.Constant;

import org.springframework.web.client.RestTemplate;

/**
 * CurrencyClient
 */
public class CurrencyClient {

    RestTemplate restTemplate = new RestTemplate();

    /**
     * Will call the api exchangin rate between currencies and return the rate based
     * on the currency. Akan mengembalikan null bila tidak menemukan currency yang
     * diberikan.
     * 
     * @param currency
     * @return object LatestBaseCurrency
     */
    public LatestBaseCurrency getRateAgaintsCurrency(String currency) throws IllegalArgumentException{
        Map<String,Object> uriVariables = new HashMap<>();
        uriVariables.put("base", currency);
        LatestBaseCurrency baseCurrencyRates = restTemplate.getForObject(Constant.CURRENCY_URI + "/latest?base={base}", LatestBaseCurrency.class, uriVariables);

        if (baseCurrencyRates.getRates() == null) {
            throw new IllegalArgumentException();
        }

        return baseCurrencyRates;
    }

    /**
     * Akan memberikan semua currency dengan rate tertentu berbasiskan euro pada
     * hari terbaru.
     * @return object LatestBaseCurrency
     */
    public LatestBaseCurrency getLatestRate() {
        LatestBaseCurrency euroCurrencyRates = restTemplate.getForObject(Constant.CURRENCY_URI + "/latest", LatestBaseCurrency.class);
        return euroCurrencyRates;
    }

    /**
     *  Return currency exchange rate based on baseCurrency and period from startDate to endDate
     * @param baseCurrency
     * @param startDate
     * @param endDate
     * @return object HistoricalBaseCurrency
     */
    public HistoricalBaseCurrency getHistoricalRateByBaseCurrencyAndDate(String baseCurrency, String startDate, String endDate) {
        HistoricalBaseCurrency historicalBaseCurrency = restTemplate.getForObject(
                createCurrencyHistoricalWithBaseUrl(baseCurrency, startDate, endDate),
                HistoricalBaseCurrency.class);
        return historicalBaseCurrency;
    }

    /**
     *
     * @param baseCurrency
     * @param startDate
     * @param endDate
     * @return url of CurrencyHistoricalWithBase
     */
    private String createCurrencyHistoricalWithBaseUrl(String baseCurrency, String startDate, String endDate) {
        StringBuilder urlBuilder =new StringBuilder();
        urlBuilder.append(createCurrencyHistoricalUrl(startDate,endDate));
        urlBuilder.append("&");
        urlBuilder.append("base=");
        urlBuilder.append(baseCurrency);
        return urlBuilder.toString();
    }

    /**
     *
     * @param startDate
     * @param endDate
     * @return HistoricalCurrency
     */
    public HistoricalBaseCurrency getHistoricalRateByDate (String startDate, String endDate) {
        HistoricalBaseCurrency historicalBaseCurrency = restTemplate.getForObject(
                createCurrencyHistoricalUrl(startDate, endDate),
                HistoricalBaseCurrency.class);
        return historicalBaseCurrency;


    }

    /**
     *
     * @param startDate
     * @param endDate
     * @return url of CurrencyHistorical
     */
    private String createCurrencyHistoricalUrl(String startDate, String endDate) {
        StringBuilder urlBuilder =new StringBuilder();
        urlBuilder.append(Constant.CURRENCY_URI + "/history?");
        urlBuilder.append("start_at=");
        urlBuilder.append(startDate);
        urlBuilder.append("&");
        urlBuilder.append("end_at=");
        urlBuilder.append(endDate);
        return urlBuilder.toString();
    }


}