package com.adf.Lab2ADF.util;

import com.adf.Lab2ADF.client.CountriesClient;
import com.adf.Lab2ADF.client.CurrencyClient;
import com.adf.Lab2ADF.client.response.GeneralResponse;
import com.adf.Lab2ADF.dataclass.currency.HistoricalBaseCurrency;
import com.adf.Lab2ADF.util.builder.GeneralResponseFactory;

import java.text.ParseException;


public class ResponseCreator {
    private static CurrencyClient currencyClient = new CurrencyClient();
    private static CountriesClient countriesClient = new CountriesClient();

    public static GeneralResponse createRegionalComparisonResponse(String region, String standardCurrency, String startDate, String endDate ) {
        try {
            countriesClient.getCountryDataByRegion(region);
        } catch (Exception e) {
            return GeneralResponseFactory.createFailedResponse(404, "daerah tidak ditemukan");
        }
        try {
            DateUtil.convertStringToDate(startDate, Constant.DATE_FORMAT);
            DateUtil.convertStringToDate(endDate, Constant.DATE_FORMAT);
        } catch (ParseException e) {
            return GeneralResponseFactory.createFailedResponse(404, "format tanggal tidak sesuai");
        }

        HistoricalBaseCurrency historicalBaseCurrency;

        try {
            historicalBaseCurrency = currencyClient.getHistoricalRateByDate(startDate, endDate);
        } catch (Exception e) {
            return GeneralResponseFactory.createFailedResponse(404, "data tanggal tidak ditemukan");
        }
        try {
            historicalBaseCurrency = currencyClient.getHistoricalRateByBaseCurrencyAndDate(standardCurrency,
                    startDate, endDate);
        }catch (Exception e) {
            return GeneralResponseFactory.createFailedResponse(404,  "mata uang tidak ditemukan");
        }

            CurrencyUtil.getRatesByRegion(countriesClient.getCountryDataByRegion(region),
                    historicalBaseCurrency);
            return GeneralResponseFactory.createOkResponse(historicalBaseCurrency);



    }
}
