package com.adf.Lab2ADF.util;

import java.util.ArrayList;
import java.util.List;

/**
 * InputParser
 */
public class Parser {

    /**
     * Will convert a string to list based on the divider of the elements in the string
     * 
     * @param elements 
     * @param divider The divider between element in the string.
     * @return
     */
    public static List<String> parseStringToList(String elements, String divider) {
        ArrayList<String> result = new ArrayList<>();
        for (String i : elements.split(divider)) {
            result.add(i);
        }
        return result;
    }

}