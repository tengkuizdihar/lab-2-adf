package com.adf.Lab2ADF.util;

import com.adf.Lab2ADF.dataclass.country.CountryData;
import com.adf.Lab2ADF.dataclass.currency.HistoricalBaseCurrency;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CurrencyUtil {

    public static void getRatesByRegion(List<CountryData> countryData, HistoricalBaseCurrency historicalBaseCurrency) {
        List<String> currencyList = getCurrenciesList(countryData);
        filterHistoricalCurrencyData(historicalBaseCurrency, currencyList);
    }

    private static void filterHistoricalCurrencyData(HistoricalBaseCurrency historicalBaseCurrency, List<String> currencyList) {
        for (String dates : historicalBaseCurrency.getDatesExchangeRates()) {
            List<String> toRemove = findUnusedCurrencyData(historicalBaseCurrency, currencyList, dates);
            removeUnusedCurrencyData(historicalBaseCurrency, dates, toRemove);
        }
    }

    private static void removeUnusedCurrencyData(HistoricalBaseCurrency historicalBaseCurrency, String dates, List<String> toRemove) {
        for(String remove : toRemove) {
            historicalBaseCurrency.getRates().get(dates).remove(remove);
        }
    }

    private static List<String> findUnusedCurrencyData(HistoricalBaseCurrency historicalBaseCurrency, List<String> currencyList, String dates) {
        List<String> toRemove = new ArrayList<>();
        for (String currency : historicalBaseCurrency.getRates().get(dates).keySet()) {
            if (!contains(currency, currencyList)) {
                toRemove.add(currency);
            }
        }
        return toRemove;
    }


    private static List<String> getCurrenciesList(List<CountryData> countryData) {
        List<String> currenciesList = countryData.stream()
                .flatMap(currencies -> currencies.getCurrencies().stream())
                .map(name -> name.getCode())
                .filter(code -> !code.equalsIgnoreCase("(none)"))
                .collect(Collectors.toList());


        return currenciesList;
    }

    private static boolean contains(String name, List<String> target) {
        boolean contain = false;
        for (String currency : target) {
            if (currency.equalsIgnoreCase(name)) {
                contain = true;
            }
        }
        return contain;
    }


}
