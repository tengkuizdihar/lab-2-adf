package com.adf.Lab2ADF.util.builder;

import java.io.Serializable;

import com.adf.Lab2ADF.client.response.GeneralResponse;

/**
 * GeneralResponseBuilder
 */
public class GeneralResponseFactory {

    public static GeneralResponse createOkResponse(Serializable message) {
        GeneralResponse returnThis = new GeneralResponse();
        returnThis.setResult(message);
        returnThis.setStatus(200);
        returnThis.setMessage("no error");
        return returnThis;
    }

	public static GeneralResponse createFailedResponse(int status, String message) {
		GeneralResponse returnThis = new GeneralResponse();
        returnThis.setStatus(status);
        returnThis.setMessage(message);
        return returnThis;
	}
}