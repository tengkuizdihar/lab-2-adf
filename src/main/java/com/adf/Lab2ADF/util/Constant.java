package com.adf.Lab2ADF.util;

/**
 * Constant
 */
public class Constant {
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String CURRENCY_URI = "https://api.exchangeratesapi.io";
    public static final String COUNTRY_URI = "https://restcountries.eu/rest/v2/";
	public static final int HISTORIC_MAXIMUM_QUERY = 3;
}